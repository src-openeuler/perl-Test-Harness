Name:           perl-Test-Harness
Epoch:          2
Version:        3.50
Release:        2
Summary:        Run Perl standard test scripts with statistics
License:        GPL-1.0-or-later or Artistic-1.0-Perl
URL:            https://metacpan.org/release/Test-Harness
Source0:        https://cpan.metacpan.org/authors/id/L/LE/LEONT/Test-Harness-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  make perl
BuildRequires:  perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
BuildRequires:  perl(CPAN::Meta::YAML)
BuildRequires:  perl(File::Temp)
Suggests:       perl(Term::ANSIColor)
Suggests:       perl(Time::HiRes)

# Filter example dependencies
%global __requires_exclude_from %{?__requires_exclude_from:%__requires_exclude_from|}^%{_datadir}/doc
%global __provides_exclude_from %{?__provides_exclude_from:%__provides_exclude_from|}^%{_datadir}/doc

%description
This package allows tests to be run and results automatically aggregated and
output to STDOUT.

%prep
%autosetup -n Test-Harness-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes Changes-2.64 README
%{perl_vendorlib}/*
%{_bindir}/*

%package_help
%files help
%doc examples
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 2:3.50-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Aug 20 2024 xuhe <xuhe@kylinos.cn> - 2:3.50-1
- Update version to 3.50
- Simplify runtests.
- Tidy up core dump file.
- Simplify injecting of @INC paths.
- Prevent double summary on bail-out.
- Prevent bail_out summary when no handler for it is defined.
- Simplify runtests for easier extension.
- Prevent double summary on bail-out.
- Prevent bail_out summary when no handler for it is defined.
- Tidy up core dump file.
- Simplify injecting of @INC paths.
- Better check for t/ directory in Perl handler.

* Tue Jan 30 2024 huyubiao <huyubiao@huawei.com> - 2:3.48-1
- update version to 3.48
  - Accept TAP version 14
  - Fix HARNESS_PERL_SWITCHES=-I handling in TAP::Harness::Env
  - Color the "ok"s as well.
  - Skip symlink tests on msys2
  - Use use absolute path for executable tests
  - Space-quote executable if has spaces
  - Avoid using Errno::EINTR directly for platforms without it
  - stop calling import on App::Prove plugins

* Wed Oct 26 2022 wangyuhang <wangyuhang27@huawei.com> - 2:3.44-1
- update version to 3.44

* Tue Apr 26 2022 renhongxun <renhongxun@h-partners.com> -2:3.43_02-2
- update epoch to 2 just for upgrading and no function changed

* Wed Jul 22 2020 dingyue <dingyue5@huawei.com> - 3.43_02-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:NA

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.43_01-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete redundant file

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.43_01-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Thu Aug 29 2019 hexiaowen <hexiaowen@huawei.com> - 3.43_01-1
- Package init
